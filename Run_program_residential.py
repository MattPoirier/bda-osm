
import os
import sys
import pandas as pd
import numpy as np
from numpy_financial import pv
from numpy_financial import npv
from numpy_financial import pmt

# ==================================================================================================================================================
# Define input file names
# ==================================================================================================================================================



HVAC_FileName = 'Stock Module (HVAC) - 2024 01 15 - S2.xlsm'
DHW_FileName = 'Stock Module (DHW) - 2024 01 15 - S2-3.xlsm'
Output_FileName = 'Outputs Module - 2023 12 20.xlsx'
Scenario = 'fossil fuel ban'

# ==================================================================================================================================================
# HVAC module
# ==================================================================================================================================================

if os.path.exists('HVAC stock database.csv'):
    HVAC_Stock_DF = pd.read_csv('HVAC stock database.csv')
    pass
else:
    # Define input parameters
    StartYear = 2021
    EndYear = 2050
    ID_Cols = ['province.territory','climate.zone','community.type','building.type','building.envelope','equipment.id']
    BaseCols = (
        ID_Cols +
        ['forecast.year','equipment.name', 'carbon.emitting.source','equipment.EUL','equipment.share','building.stock','demolition.rate',
         'equipment.stock.existing','equipment.stock.new'])

    # Define input file
    FileName = os.path.join('Input',HVAC_FileName)

    # Extract all relevant databases from input file
    EquipmentList_DF = pd.read_excel(FileName,sheet_name='Equipment List')
    REMSS_DF = pd.read_excel(FileName,sheet_name='REMSS')
    NBEMSS_DF = pd.read_excel(FileName,sheet_name='NBEMSS')
    CurrBuildStock_DF = pd.read_excel(FileName,sheet_name='Existing Building Stock')
    EquipmentDist_DF = pd.read_excel(FileName,sheet_name='Equipment Distribution')
    BuildDemoRates_DF = pd.read_excel(FileName,sheet_name='Building Demolition Rates')
    RetrofitRates_DF = pd.read_excel(FileName,sheet_name='Envelope Retrofit Rates')
    BuildConstSchedule_DF = pd.read_excel(FileName,sheet_name='Building Construction Schedule')

    # Prepare equipment distribution database for merging
    EquipmentDist_DF = pd.melt(
        EquipmentDist_DF,
        id_vars=['province.territory','community.type','building.type'],
        var_name='equipment.id',value_name='equipment.share')

    # Create master database by merging the above two databases
    Master_DF = EquipmentDist_DF.merge(CurrBuildStock_DF)
    


    # Merge equipment list and demolition rate databases with master database
    Master_DF = Master_DF.merge(EquipmentList_DF)

    
    Master_DF = Master_DF.merge(BuildDemoRates_DF)



    # Define forecast year column so that annual databases can later on be merged with master database
    Master_DF['forecast.year'] = StartYear

    # Initialize existing and new stock for the simulation start year
    Master_DF['equipment.stock.existing'] = Master_DF['building.stock']*Master_DF['equipment.share']
    Master_DF['equipment.stock.new'] = 0
    


    # Extend master database up to the simulation end year
    Tmp_DF = Master_DF.copy()
    for Year in range(StartYear + 1,EndYear + 1):
        Tmp_DF['forecast.year'] = Year
        Master_DF = pd.concat([Master_DF,Tmp_DF])
        pass


    # Initialize the retirement schedule
    Tmp_DF = Master_DF[Master_DF['forecast.year'] == StartYear]
    Tmp_DF['latest.retirement.year'] = Tmp_DF['forecast.year'] + Tmp_DF['equipment.EUL']
    # Reduce table size to ID_Cols and retirement year
    Tmp_DF = Tmp_DF[ID_Cols + ['latest.retirement.year']]
    Master_DF = Master_DF.merge(Tmp_DF,how='left')
    Master_DF['equipment.retirements'] = np.where(
        (Master_DF['forecast.year'] > StartYear) & (Master_DF['forecast.year'] <= Master_DF['latest.retirement.year']),
        (Master_DF['equipment.stock.existing']/Master_DF['equipment.EUL']*
         (1.0 - Master_DF['demolition.rate'])**(Master_DF['forecast.year'] - StartYear)),0)
    Master_DF = Master_DF.drop(columns=['latest.retirement.year'])
    
    # Prepare retrofits database to be merged with master data during annual simulation
    RetrofitRates_DF = RetrofitRates_DF.drop(columns=['building.vintage'])
    RetrofitRates_DF = RetrofitRates_DF.rename(columns={'building.envelope.current':'building.envelope'})
    RetrofitRates_DF = pd.pivot_table(
        RetrofitRates_DF,
        index=['province.territory','climate.zone','building.type','building.envelope','forecast.year'],
        columns='building.envelope.upgrade',values='retrofit.rate')
    RetrofitsTo_List = RetrofitRates_DF.columns.tolist()
    RetrofitRates_DF.columns = ['retrofit.rate.to.' + Item for Item in RetrofitRates_DF.columns]
    RetrofitRates_DF = RetrofitRates_DF.reset_index().fillna(0)

    # Prepare REMSS_DF database to be merged with master data during annual simulation
    REMSS_DF = REMSS_DF.drop(columns=['equipment.name.retiring','equipment.name.replacement'])
    REMSS_DF = pd.melt(REMSS_DF,
                       id_vars=['province.territory','climate.zone','community.type','building.type','equipment.id.retiring','equipment.id.replacement'],
                       var_name='forecast.year',value_name='replacement.rate')
    REMSS_DF = REMSS_DF.rename(columns={'equipment.id.retiring':'equipment.id'})
    REMSS_DF = pd.pivot_table(REMSS_DF,
                              index=['province.territory','climate.zone','community.type','building.type','equipment.id','forecast.year'],
                              columns='equipment.id.replacement',values='replacement.rate')
    ReplaceWith_List = REMSS_DF.columns.tolist()
    REMSS_DF.columns = ['replacement.rate.to.' + Item for Item in REMSS_DF.columns]
    REMSS_DF = REMSS_DF.reset_index().fillna(0)

    # Merge new contruction and NBEMSS_DF data with master data
    BuildConstSchedule_DF = BuildConstSchedule_DF.drop(columns=['building.vintage'])

    # Prepare NBEMSS_DF database to be merged with master data during annual simulation
    NBEMSS_DF = NBEMSS_DF.drop(columns=['equipment.name'])
    NBEMSS_DF = pd.melt(NBEMSS_DF,
                        id_vars=['province.territory','climate.zone','community.type','building.type','equipment.id'],
                        var_name='forecast.year',value_name='adoption.rate')

    # Start annual simulation
    Master_DF = Master_DF.drop(columns=['building.vintage'])
    Tmp_DF = Master_DF[Master_DF['forecast.year'] == StartYear]
    
    
    for Year in range(StartYear + 1,EndYear + 1):
        print('Simulating year ' + str(Year) + '...')

        # Prepare sub-database for current year
        Tmp_DF = Tmp_DF[BaseCols]
        Tmp_DF['forecast.year'] = Year
        TmpMaster_DF = Master_DF.loc[Master_DF['forecast.year'] == Year,ID_Cols + ['equipment.retirements']]
        Tmp_DF = Tmp_DF.merge(TmpMaster_DF,how='left')
        
        # Remove sub-data from master database so that the updated information will be appended later
        Master_DF = Master_DF[Master_DF['forecast.year'] != Year]

        # Merge annual data to current year's sub-database
        Tmp_DF = Tmp_DF.merge(RetrofitRates_DF,how='left').fillna(0)
        Tmp_DF = Tmp_DF.merge(REMSS_DF,how='left').fillna(0)
        Tmp_DF = Tmp_DF.merge(BuildConstSchedule_DF,how='left').fillna(0)
        Tmp_DF = Tmp_DF.merge(NBEMSS_DF,how='left').fillna(0)
    
        # Calculate building demolitions
        Tmp_DF['building.demolitions'] = Tmp_DF['equipment.stock.existing']*Tmp_DF['demolition.rate']
    
        # Calculate retrofits in and out
        Tmp_DF['retrofits.out'] = 0
        for Envelope in RetrofitsTo_List:
            Tmp_DF['retrofits.to.' + Envelope] = Tmp_DF['equipment.retirements']*Tmp_DF['retrofit.rate.to.' + Envelope]
            Tmp_DF['retrofits.out'] = Tmp_DF['retrofits.out'] + Tmp_DF['retrofits.to.' + Envelope]
            pass
        RetrofitsIn_Tmp = Tmp_DF[
            ['province.territory','climate.zone','community.type','building.type','equipment.id'] +
            ['retrofits.to.' + Item for Item in RetrofitsTo_List]]
        RetrofitsIn_Tmp = RetrofitsIn_Tmp.groupby(
            ['province.territory','climate.zone','community.type','building.type','equipment.id']).sum().reset_index()
        for Envelope in RetrofitsTo_List:
            RetrofitsIn_Tmp = RetrofitsIn_Tmp.rename(columns={'retrofits.to.' + Envelope:Envelope})
            pass
        RetrofitsIn_Tmp = pd.melt(
            RetrofitsIn_Tmp,
            id_vars=['province.territory','climate.zone','community.type','building.type','equipment.id'],
            var_name='building.envelope',value_name='retrofits.in')
        Tmp_DF = Tmp_DF.merge(RetrofitsIn_Tmp,how='left').fillna(0)
        Tmp_DF = Tmp_DF.drop(columns=['retrofit.rate.to.' + Envelope for Envelope in RetrofitsTo_List])
        Tmp_DF = Tmp_DF.drop(columns=['retrofits.to.' + Envelope for Envelope in RetrofitsTo_List])
    
        # Calculate replacements in and out
        Tmp_DF['equipment.replacements'] = (
            Tmp_DF['equipment.retirements'] + Tmp_DF['retrofits.in'] - Tmp_DF['retrofits.out'])
        for Equipment in ReplaceWith_List:
            Tmp_DF['replacements.to.' + Equipment] = (
                Tmp_DF['equipment.replacements']*Tmp_DF['replacement.rate.to.' + Equipment])
            pass
        ReplacementsIn_Tmp = Tmp_DF[
            ['province.territory','climate.zone','community.type','building.type','building.envelope'] +
            ['replacements.to.' + Item for Item in ReplaceWith_List]]
        ReplacementsIn_Tmp = ReplacementsIn_Tmp.groupby(
            ['province.territory','climate.zone','community.type','building.type','building.envelope']).sum().reset_index()
        for Equipment in ReplaceWith_List:
            ReplacementsIn_Tmp = ReplacementsIn_Tmp.rename(columns={'replacements.to.' + Equipment:Equipment})
            pass
        ReplacementsIn_Tmp = pd.melt(
            ReplacementsIn_Tmp,
            id_vars=['province.territory','climate.zone','community.type','building.type','building.envelope'],
            var_name='equipment.id',value_name='equipment.additions.existing')
        Tmp_DF = Tmp_DF.merge(ReplacementsIn_Tmp,how='left').fillna(0)
        Tmp_DF = Tmp_DF.drop(columns=['replacement.rate.to.' + Equipment for Equipment in ReplaceWith_List])
        Tmp_DF = Tmp_DF.drop(columns=['replacements.to.' + Equipment for Equipment in ReplaceWith_List])
    
        # Calculate new and total equipment additions
        Tmp_DF['equipment.additions.new'] = Tmp_DF['building.additions']*Tmp_DF['adoption.rate']
        Tmp_DF['equipment.additions'] = Tmp_DF['equipment.additions.existing'] + Tmp_DF['equipment.additions.new']
        
        # Save the retirement year of new additions for updating the retirement schedule later on
        Tmp_DF['new.equipment.retirement.year'] = Year + Tmp_DF['equipment.EUL']
        
        # Update existing and new equipment stock
        Tmp_DF['equipment.stock.existing'] = (
            Tmp_DF['equipment.stock.existing'] - Tmp_DF['building.demolitions'] - Tmp_DF['equipment.retirements'] +
            Tmp_DF['equipment.additions.existing'] + Tmp_DF['equipment.stock.new']*(1.0 - Tmp_DF['demolition.rate']))
        Tmp_DF['equipment.stock.new'] = Tmp_DF['equipment.additions.new']
    
        # Combine current year's sub-database with master database 
        Master_DF = pd.concat([Master_DF,Tmp_DF])
    
        # Update retirements schedule
        NewRetirements_DF = Tmp_DF[ID_Cols + ['new.equipment.retirement.year','equipment.additions']]
        NewRetirements_DF = NewRetirements_DF[NewRetirements_DF['new.equipment.retirement.year'] <= EndYear]
        NewRetirements_DF = NewRetirements_DF.rename(columns={'new.equipment.retirement.year':'forecast.year','equipment.additions':'new.retirements'})
        Master_DF = Master_DF.merge(NewRetirements_DF,how='left').fillna(0)
        Master_DF['equipment.retirements'] = Master_DF['equipment.retirements'] + Master_DF['new.retirements']*(1.0 - Master_DF['demolition.rate'])**(Master_DF['forecast.year'] - Year)
        Master_DF = Master_DF.drop(columns=['new.retirements'])
        pass
    
    # Melt equipment additions into existing and new categories
    Tmp_DF = Master_DF.rename(columns={'equipment.additions.existing':'existing','equipment.additions.new':'new'})
    Tmp_DF = Tmp_DF.drop(columns=['equipment.additions'])
    Tmp_DF = pd.melt(
        Tmp_DF,
        id_vars=ID_Cols + ['forecast.year'],
        value_vars=['existing','new'],
        var_name='building.vintage',
        value_name='equipment.additions')
    
    # Melt equipment stock into existing and new categories
    Master_DF = Master_DF.rename(columns={'equipment.stock.existing':'existing','equipment.stock.new':'new'})
    OutputCols = [
        'forecast.year','province.territory','climate.zone','community.type','building.type',
        'building.envelope','equipment.name','equipment.id','carbon.emitting.source','equipment.EUL','building.demolitions',
        'equipment.retirements','equipment.replacements']
    Master_DF = pd.melt(
        Master_DF,
        id_vars=OutputCols,
        value_vars=['existing','new'],
        var_name='building.vintage',
        value_name='equipment.stock')
    Master_DF = Master_DF.merge(Tmp_DF,how='left')

    # Clear demolitions, retirements, and replacements for new buildings
    Master_DF.loc[
        Master_DF['building.vintage'] == 'new',
        ['building.demolitions','equipment.retirements','equipment.replacements']] = 0
    
    # Create scenario, sector, and equipment class for the master database
    Master_DF['scenario'] = Scenario
    Master_DF['sector'] = 'residential'
    Master_DF['equipment.class'] = 'HVAC'

    # Create final HVAC database from master database
    OutputCols = [
        'scenario','forecast.year','province.territory','climate.zone','community.type','sector','building.type','building.vintage',
        'building.envelope','equipment.id','equipment.name','equipment.class','carbon.emitting.source', 'equipment.EUL','building.demolitions',
        'equipment.retirements','equipment.additions','equipment.stock']
 
    HVAC_Stock_DF = Master_DF[OutputCols]
    

    pass



# ==================================================================================================================================================
# Building stock module
# ==================================================================================================================================================

if os.path.exists('Building stock.csv'):
    BuildingStock_DF = pd.read_csv('Building stock.csv')
    pass
else:
    BuildingStock_DF = HVAC_Stock_DF[
        ['forecast.year','province.territory','climate.zone','community.type','building.type',
         'building.vintage','building.envelope','equipment.stock']]
    BuildingStock_DF = BuildingStock_DF.groupby(
        ['forecast.year','province.territory','climate.zone','community.type','building.type',
         'building.vintage','building.envelope']).sum().reset_index()
    
    BuildingStock_DF = BuildingStock_DF.rename(
        columns={'equipment.stock':'building.stock'})

    BuildingStock_DF.to_csv('Building stock.csv',index=False)
    pass

# ==================================================================================================================================================
# DHW module
# ==================================================================================================================================================

if os.path.exists('DHW stock database.csv'):
    DHW_Stock_DF = pd.read_csv('DHW stock database.csv')
    pass
else:
    StartYear = 2021
    EndYear = 2050
    ID_Cols = [
        'province.territory','climate.zone','community.type','building.type','building.vintage','building.envelope',
        'equipment.id']
    BaseCols = (
        ID_Cols +
        ['forecast.year','equipment.name','carbon.emitting.source','equipment.EUL','equipment.share','demolition.rate','equipment.stock',
         'equipment.stock.uncalibrated'])
    
    FileName = os.path.join('Input',HVAC_FileName)
    BuildConstSchedule_DF = pd.read_excel(FileName,sheet_name='Building Construction Schedule')
    
    FileName = os.path.join('Input',DHW_FileName)
    EquipmentList_DF = pd.read_excel(FileName,sheet_name='Equipment List')
    REMSS_DF = pd.read_excel(FileName,sheet_name='REMSS')
    NBEMSS_DF = pd.read_excel(FileName,sheet_name='NBEMSS')
    EquipmentDist_DF = pd.read_excel(FileName,sheet_name='Equipment Distribution')
    BuildDemoRates_DF = pd.read_excel(FileName,sheet_name='Building Demolition Rates')

    EquipmentDist_DF = EquipmentDist_DF.drop(columns=['building.vintage'])
    EquipmentDist_DF = pd.melt(
        EquipmentDist_DF,
        id_vars=['province.territory','climate.zone','community.type','building.type'],
        var_name='equipment.id',value_name='equipment.share')

    Master_DF = BuildingStock_DF.merge(EquipmentDist_DF,how='left').fillna(0)
    Master_DF = Master_DF.merge(EquipmentList_DF,how='left').fillna(0)
    BuildDemoRates_DF = BuildDemoRates_DF.drop(columns=['building.vintage'])
    Master_DF = Master_DF.merge(BuildDemoRates_DF,how='left').fillna(0)

    Master_DF['equipment.stock'] = np.where(
        (Master_DF['forecast.year'] == StartYear) & (Master_DF['building.vintage'] == 'existing'),
        Master_DF['building.stock']*Master_DF['equipment.share'],0)
    Master_DF['equipment.stock.uncalibrated'] = Master_DF['equipment.stock']

    Tmp_DF = Master_DF[(Master_DF['forecast.year'] == StartYear) & (Master_DF['building.vintage'] == 'existing')]
    Tmp_DF['retirement.year'] = StartYear + Tmp_DF['equipment.EUL']
    Tmp_DF = Tmp_DF.rename(columns={'equipment.stock':'initial.equipment.stock'})
    Tmp_DF = Tmp_DF[ID_Cols + ['retirement.year','initial.equipment.stock']]
    Master_DF = Master_DF.merge(Tmp_DF,how='left').fillna(0)
    Master_DF['equipment.retirements'] = np.where(
        (Master_DF['forecast.year'] > StartYear) & (Master_DF['forecast.year'] <= Master_DF['retirement.year']) &
        (Master_DF['building.vintage'] == 'existing'),
        (Master_DF['initial.equipment.stock']/Master_DF['equipment.EUL']*
         (1.0 - Master_DF['demolition.rate'])**(Master_DF['forecast.year'] - StartYear)),0)
    Master_DF = Master_DF.drop(columns=['retirement.year','initial.equipment.stock'])

    REMSS_DF = REMSS_DF.drop(columns=['equipment.name.retiring','equipment.name.replacement'])
    REMSS_DF = pd.melt(
        REMSS_DF,
        id_vars=['province.territory','climate.zone','community.type','building.type','equipment.id.retiring','equipment.id.replacement'],
        var_name='forecast.year',value_name='replacement.rate')
    REMSS_DF = REMSS_DF.rename(columns={'equipment.id.retiring':'equipment.id'})
    REMSS_DF = pd.pivot_table(
        REMSS_DF,
        index=['province.territory','climate.zone','community.type','building.type','equipment.id','forecast.year'],
        columns='equipment.id.replacement',
        values='replacement.rate')
    ReplaceWith_List = REMSS_DF.columns.tolist()
    REMSS_DF.columns = ['replacement.rate.to.' + Item for Item in REMSS_DF.columns]
    REMSS_DF = REMSS_DF.reset_index().fillna(0)

    NBEMSS_DF = NBEMSS_DF.drop(columns=['equipment.name'])
    NBEMSS_DF = pd.melt(
        NBEMSS_DF,
        id_vars=['province.territory','climate.zone','community.type','building.type','equipment.id'],
        var_name='forecast.year',
        value_name='adoption.rate')

    Tmp_DF = Master_DF[Master_DF['forecast.year'] == StartYear]
    for Year in range(StartYear + 1,EndYear + 1):
        print('Simulating year ' + str(Year) + '...')

        Tmp_DF = Tmp_DF[BaseCols]
        Tmp_DF['forecast.year'] = Year
        TmpMaster_DF = Master_DF.loc[
            Master_DF['forecast.year'] == Year,ID_Cols + ['equipment.retirements','building.stock']]
        Tmp_DF = Tmp_DF.merge(TmpMaster_DF,how='left')
    
        Master_DF = Master_DF[Master_DF['forecast.year'] != Year]

        Tmp_DF = Tmp_DF.merge(REMSS_DF,how='left').fillna(0)
        Tmp_DF = Tmp_DF.merge(BuildConstSchedule_DF,how='left').fillna(0)
        Tmp_DF = Tmp_DF.merge(NBEMSS_DF,how='left').fillna(0)
    
        Tmp_DF['building.demolitions'] = np.where(
            Tmp_DF['building.vintage'] == 'existing',Tmp_DF['equipment.stock']*Tmp_DF['demolition.rate'],0)
    
        for Equipment in ReplaceWith_List:
            Tmp_DF['replacements.to.' + Equipment] = (
                Tmp_DF['equipment.retirements']*Tmp_DF['replacement.rate.to.' + Equipment])
            pass
        ReplacementsIn_Tmp = Tmp_DF.loc[
            Tmp_DF['building.vintage'] == 'existing',
            ['province.territory','climate.zone','community.type','building.type','building.vintage',
             'building.envelope'] + ['replacements.to.' + Item for Item in ReplaceWith_List]]
        ReplacementsIn_Tmp = ReplacementsIn_Tmp.groupby(
            ['province.territory','climate.zone','community.type','building.type','building.vintage',
             'building.envelope']).sum().reset_index()
        for Equipment in ReplaceWith_List:
            ReplacementsIn_Tmp = ReplacementsIn_Tmp.rename(columns={'replacements.to.' + Equipment:Equipment})
            pass
        ReplacementsIn_Tmp = pd.melt(
            ReplacementsIn_Tmp,
            id_vars=['province.territory','climate.zone','community.type','building.type','building.vintage',
                     'building.envelope'],
            var_name='equipment.id',
            value_name='equipment.additions')
        Tmp_DF = Tmp_DF.merge(ReplacementsIn_Tmp,how='left').fillna(0)
        Tmp_DF = Tmp_DF.drop(columns=['replacement.rate.to.' + Equipment for Equipment in ReplaceWith_List])
        Tmp_DF = Tmp_DF.drop(columns=['replacements.to.' + Equipment for Equipment in ReplaceWith_List])
    
        Tmp_DF['equipment.additions'] = np.where(
            Tmp_DF['building.vintage'] == 'new',
            Tmp_DF['building.stock']*Tmp_DF['adoption.rate'],
            Tmp_DF['equipment.additions'])
        Tmp_DF['retirement.year'] = Year + Tmp_DF['equipment.EUL']
    
        NewTmp_DF = Tmp_DF.loc[Tmp_DF['building.vintage'] == 'new',ID_Cols + ['equipment.stock']]
        NewTmp_DF['building.vintage'] = 'existing'
        NewTmp_DF = NewTmp_DF.rename(columns={'equipment.stock':'equipment.stock.new'})
        Tmp_DF = Tmp_DF.merge(NewTmp_DF,how='left')
        Tmp_DF['equipment.stock'] = np.where(
            Tmp_DF['building.vintage'] == 'existing',
            (Tmp_DF['equipment.stock'] - Tmp_DF['building.demolitions'] - Tmp_DF['equipment.retirements'] +
             Tmp_DF['equipment.additions'] + Tmp_DF['equipment.stock.new']*(1.0 - Tmp_DF['demolition.rate'])),
             Tmp_DF['equipment.additions'])
        Tmp_DF['equipment.stock.uncalibrated'] = Tmp_DF['equipment.stock']
        Tmp_DF = Tmp_DF.drop(columns=['equipment.stock.new'])

        TmpStock_A_DF = Tmp_DF.loc[
            Tmp_DF['building.vintage'] == 'existing',
            ['province.territory','climate.zone','community.type','building.type','building.vintage','equipment.stock']]
        TmpStock_A_DF = TmpStock_A_DF.rename(columns={'equipment.stock':'all.equipment.stock'})
        TmpStock_A_DF = TmpStock_A_DF.groupby(
            ['province.territory','climate.zone','community.type','building.type','building.vintage']).sum().reset_index()
        TmpStock_B_DF = Tmp_DF.loc[
            Tmp_DF['building.vintage'] == 'existing',
            ['province.territory','climate.zone','community.type','building.type','building.vintage',
             'equipment.id','equipment.stock']]
        TmpStock_B_DF = TmpStock_B_DF.rename(columns={'equipment.stock':'specific.equipment.stock'})
        TmpStock_B_DF = TmpStock_B_DF.groupby(
            ['province.territory','climate.zone','community.type','building.type','building.vintage',
             'equipment.id']).sum().reset_index()
        TmpStock_DF = TmpStock_B_DF.merge(TmpStock_A_DF,how='left')
        Tmp_DF = Tmp_DF.merge(TmpStock_DF,how='left')
        

        Tmp_DF['equipment.share'] = np.where(
            Tmp_DF['building.vintage'] == 'existing',
            Tmp_DF['specific.equipment.stock']/Tmp_DF['all.equipment.stock'],Tmp_DF['equipment.share'])
        Tmp_DF = Tmp_DF.drop(columns=['all.equipment.stock','specific.equipment.stock'])
    
        Tmp_DF['equipment.stock'] = np.where(
            Tmp_DF['building.vintage'] == 'existing',
            Tmp_DF['building.stock']*Tmp_DF['equipment.share'],Tmp_DF['equipment.stock'])

        Master_DF = pd.concat([Master_DF,Tmp_DF])
    
        NewRetirements_DF = Tmp_DF[ID_Cols + ['retirement.year','equipment.additions']]
        NewRetirements_DF = NewRetirements_DF.drop(columns=['building.vintage'])
        NewRetirements_DF = NewRetirements_DF.groupby(
            list(NewRetirements_DF.columns.difference(['equipment.additions']))).sum().reset_index()
        NewRetirements_DF = NewRetirements_DF[NewRetirements_DF['retirement.year'] <= EndYear]
        NewRetirements_DF = NewRetirements_DF.rename(
            columns={'retirement.year':'forecast.year','equipment.additions':'new.retirements'})
        NewRetirements_DF['building.vintage'] = 'existing'
        Master_DF = Master_DF.merge(NewRetirements_DF,how='left').fillna(0)
        Master_DF['equipment.retirements'] = (
            Master_DF['equipment.retirements'] +
            Master_DF['new.retirements']*(1.0 - Master_DF['demolition.rate'])**(Master_DF['forecast.year'] - Year))
        Master_DF = Master_DF.drop(columns=['retirement.year','new.retirements'])
        pass

    Master_DF['scenario'] = Scenario
    Master_DF['sector'] = 'residential'
    Master_DF['equipment.class'] = 'DHW'

    OutputCols = ['scenario','forecast.year','province.territory','climate.zone','community.type','sector','building.type','building.vintage',
        'building.envelope','equipment.id','equipment.name','equipment.class','carbon.emitting.source','equipment.EUL','building.demolitions',
        'equipment.retirements','equipment.additions','equipment.stock']
    DHW_Stock_DF = Master_DF[OutputCols]

    pass

# ==================================================================================================================================================
# Output module
# ==================================================================================================================================================

# Define input file name
FileName = os.path.join('Input',Output_FileName)

# Extract all relevant databases from input file
EquipmentList_DF = pd.read_excel(FileName,sheet_name='Equipment List')
EquipmentLoadShapes_DF = pd.read_excel(FileName,sheet_name='Equipment Loadshapes')
BuildingLoadShapes_DF = pd.read_excel(FileName,sheet_name='Building Loadshapes')
BuildingPeakHours_DF = pd.read_excel(FileName,sheet_name='Building Peak Hours')
BaseLoads_DF = pd.read_excel(FileName,sheet_name='Building Base Loads')
Stock_DF = pd.concat([HVAC_Stock_DF,DHW_Stock_DF])
EquipmentChar_DF = pd.read_excel(FileName,sheet_name='Equipment Characterization')
DiscountRates_DF = pd.read_excel(FileName,sheet_name='Discount Rates')
RetailRates_DF = pd.read_excel(FileName,sheet_name='Retail Rates')
ResourceCosts_DF = pd.read_excel(FileName,sheet_name='Resource Costs')
SystemInputs_DF = pd.read_excel(FileName,sheet_name='System Inputs')
OtherInputs_DF = pd.read_excel(FileName,sheet_name='Other Inputs')
EquipmentLoadShapes_DF = pd.melt(EquipmentLoadShapes_DF,
                                 id_vars=['hour.of.year','month.of.year','day.of.month','hour.of.day'],
                                 var_name='equipment.load.shape',value_name='coincidence.factor.equipment')
BuildingLoadShapes_DF = pd.melt(BuildingLoadShapes_DF,
                                id_vars=['hour.of.year','month.of.year','day.of.month','hour.of.day'],
                                var_name='building.load.shape',value_name='building.coincidence.factor')

# Customer coincidence factors
Tmp_DF = BuildingPeakHours_DF.rename(columns={'building.monthly.peak.hour.of.year':'hour.of.year'})
CustomerCoincidenceFact_DF = EquipmentLoadShapes_DF.merge(Tmp_DF)
CustomerCoincidenceFact_DF = CustomerCoincidenceFact_DF[
    ['building.load.shape','equipment.load.shape','coincidence.factor.equipment']]
CustomerCoincidenceFact_DF = CustomerCoincidenceFact_DF.groupby(
    ['building.load.shape','equipment.load.shape']).sum().reset_index()
CustomerCoincidenceFact_DF = CustomerCoincidenceFact_DF.rename(
    columns={'coincidence.factor.equipment':'coincidence.factor.customer'})

CustomerCoincidenceFact_DF.to_csv('Customer coincidence factors.csv',index=False)

# Output table annual - Part I
if os.path.exists('Output table annual.csv'):
    OutputTableAnnual_DF = pd.read_csv('Output table annual.csv')
    pass
else:
    OutputTableAnnual_DF = pd.concat([HVAC_Stock_DF,DHW_Stock_DF])
    OutputTableAnnual_DF.to_csv('Combined stock database.csv',index=False)
    
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(BaseLoads_DF)
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(
        EquipmentList_DF.drop(columns=['equipment.name','equipment.EUL']))
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(
        EquipmentChar_DF.drop(columns=['equipment.name','equipment.type','equipment.EUL']))
    OutputTableAnnual_DF = OutputTableAnnual_DF.rename(
        columns={'equipment.stock':'equipment.stock.total',
                 'equipment.additions':'equipment.stock.new'})
    
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(CustomerCoincidenceFact_DF)
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(DiscountRates_DF)
    
    OutputTableAnnual_DF = OutputTableAnnual_DF.rename(
        columns={'equipment.energy.gas':'energy.gas.site.per.unit',
                 'equipment.energy.oil':'energy.oil.site.per.unit',
                 'equipment.energy.propane':'energy.propane.site.per.unit',
                 'equipment.energy.biomass':'energy.biomass.site.per.unit',
                 'equipment.energy.electricity':'energy.electricity.site.per.unit'})
    
    OutputTableAnnual_DF['energy.gas.site.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['energy.gas.site.per.unit'])
    OutputTableAnnual_DF['energy.oil.site.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['energy.oil.site.per.unit'])
    OutputTableAnnual_DF['energy.propane.site.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['energy.propane.site.per.unit'])
    OutputTableAnnual_DF['energy.biomass.site.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['energy.biomass.site.per.unit'])
    OutputTableAnnual_DF['energy.electricity.site.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['energy.electricity.site.per.unit'])
    OutputTableAnnual_DF['energy.total.site.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.site.total.stock']+OutputTableAnnual_DF['energy.oil.site.total.stock']+OutputTableAnnual_DF['energy.propane.site.total.stock']+OutputTableAnnual_DF['energy.biomass.site.total.stock']+OutputTableAnnual_DF['energy.electricity.site.total.stock'])
    
    OutputTableAnnual_DF['demand.electricity.site.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.site.total.stock']*
        OutputTableAnnual_DF['coincidence.factor.customer']/0.0036)
    
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(SystemInputs_DF)
    OutputTableAnnual_DF['energy.gas.source.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.site.total.stock']/
        (1.0 - OutputTableAnnual_DF['system_loss.gas']))
    OutputTableAnnual_DF['energy.oil.source.total.stock'] = OutputTableAnnual_DF['energy.oil.site.total.stock']
    OutputTableAnnual_DF['energy.propane.source.total.stock'] = OutputTableAnnual_DF['energy.propane.site.total.stock']
    OutputTableAnnual_DF['energy.biomass.source.total.stock'] = OutputTableAnnual_DF['energy.biomass.site.total.stock']
    OutputTableAnnual_DF['energy.electricity.source.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.site.total.stock']/
        (1.0 - OutputTableAnnual_DF['system_loss.electricity']))
    pass

# System hourly load - Part I
SystemHourlyLoad_DF = Stock_DF[['province.territory','building.type','equipment.class','forecast.year',
                                'equipment.stock']]
SystemHourlyLoad_DF = SystemHourlyLoad_DF.groupby(
    ['province.territory','building.type','equipment.class','forecast.year']).sum().reset_index()

SystemHourlyLoad_DF = SystemHourlyLoad_DF[SystemHourlyLoad_DF['equipment.class'] == 'HVAC']

SystemHourlyLoad_DF = SystemHourlyLoad_DF.rename(columns={'equipment.stock':'building.stock'})

SystemHourlyLoad_DF = SystemHourlyLoad_DF.merge(BaseLoads_DF)

Cols2Keep = ['province.territory','forecast.year','system_loss.electricity']
SystemHourlyLoad_DF = SystemHourlyLoad_DF.merge(SystemInputs_DF[Cols2Keep])

SystemHourlyLoad_DF['energy.electricity.source.base.load'] = (
    SystemHourlyLoad_DF['building.stock']*SystemHourlyLoad_DF['building.energy.electricity']
    /(1.0 - SystemHourlyLoad_DF['system_loss.electricity']))

SystemHourlyLoad_DF = SystemHourlyLoad_DF.merge(BuildingLoadShapes_DF)

SystemHourlyLoad_DF['building.demand.electricity.source.total.stock'] = (
    SystemHourlyLoad_DF['energy.electricity.source.base.load']*SystemHourlyLoad_DF['building.coincidence.factor']
    /0.0036)

SystemHourlyLoad_DF = SystemHourlyLoad_DF[
    ['province.territory','forecast.year','hour.of.year',
     'building.demand.electricity.source.total.stock']]
SystemHourlyLoad_DF = SystemHourlyLoad_DF.groupby(
    ['province.territory','forecast.year','hour.of.year']).sum().reset_index()

Tmp_DF = OutputTableAnnual_DF[['province.territory','equipment.load.shape','forecast.year',
                               'energy.electricity.source.total.stock']]


Tmp_DF = Tmp_DF.groupby(['province.territory','equipment.load.shape','forecast.year']).sum().reset_index()

SystemHourlyLoad_DF.to_csv('System Hourly Load.csv',index=False)

SystemHourlyLoad_DF = SystemHourlyLoad_DF.merge(Tmp_DF)

SystemHourlyLoad_DF = SystemHourlyLoad_DF.merge(EquipmentLoadShapes_DF)

SystemHourlyLoad_DF['equipment.demand.electricity.source.total.stock'] = (
    SystemHourlyLoad_DF['energy.electricity.source.total.stock']*SystemHourlyLoad_DF['coincidence.factor.equipment']
    /0.0036)

SystemHourlyLoad_DF = SystemHourlyLoad_DF[['province.territory','forecast.year','hour.of.year',
                                           'building.demand.electricity.source.total.stock',
                                           'equipment.demand.electricity.source.total.stock']]
SystemHourlyLoad_DF = SystemHourlyLoad_DF.groupby(
    ['province.territory','forecast.year','hour.of.year',
     'building.demand.electricity.source.total.stock']).sum().reset_index()

SystemHourlyLoad_DF['system.demand.electricity'] = (
    SystemHourlyLoad_DF['building.demand.electricity.source.total.stock']
    + SystemHourlyLoad_DF['equipment.demand.electricity.source.total.stock'])

SystemHourlyLoad_DF.to_csv('System hourly load.csv',index=False)

# System peak demand hour
SystemHourlyLoad_DF = SystemHourlyLoad_DF.reset_index(drop=True)
SysPeakHourDemand_DF = SystemHourlyLoad_DF.loc[
    SystemHourlyLoad_DF.groupby(
        ['province.territory','forecast.year'])['system.demand.electricity'].idxmax()]
SysPeakHourDemand_DF = SysPeakHourDemand_DF.rename(
    columns={'hour.of.year':'system.peak.hour.of.year',
             'building.demand.electricity.source.total.stock':'peak.building.demand.electricity',
             'equipment.demand.electricity.source.total.stock':'peak.equipment.demand.electricity',
             'system.demand.electricity':'peak.system.demand.electricity'})
SysPeakHourDemand_DF = SysPeakHourDemand_DF[
    ['province.territory','forecast.year','system.peak.hour.of.year',
     'peak.building.demand.electricity','peak.equipment.demand.electricity','peak.system.demand.electricity']]

SysPeakHourDemand_DF.to_csv('System peak demand hour.csv',index=False)

# System coincidence factors
Tmp_DF = SysPeakHourDemand_DF.rename(columns={'system.peak.hour.of.year':'hour.of.year'})
SystemCoincidenceFact_DF = EquipmentLoadShapes_DF.merge(Tmp_DF)

SystemCoincidenceFact_DF = SystemCoincidenceFact_DF.rename(
    columns={'coincidence.factor.equipment':'coincidence.factor.system'})

SystemCoincidenceFact_DF = SystemCoincidenceFact_DF[
    ['province.territory','equipment.load.shape','forecast.year','coincidence.factor.system']]
SystemCoincidenceFact_DF = SystemCoincidenceFact_DF.groupby(
    ['province.territory','equipment.load.shape','forecast.year']).sum().reset_index()
SystemCoincidenceFact_DF = pd.pivot_table(
    SystemCoincidenceFact_DF,
    index=['province.territory','equipment.load.shape'],
    columns='forecast.year',
    values='coincidence.factor.system').reset_index()

for Year in range(2050 + 1,2080 + 1):
    SystemCoincidenceFact_DF[Year] = SystemCoincidenceFact_DF[2050]
    pass

SystemCoincidenceFact_DF.to_csv('System coincidence factors.csv',index=False)

# Output table annual - Part II
if os.path.exists('Output table annual.csv'):
    OutputTableAnnual_DF = pd.read_csv('Output table annual.csv')
    pass
else:
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(SysPeakHourDemand_DF)
    Tmp_DF = EquipmentLoadShapes_DF.rename(columns={'hour.of.year':'system.peak.hour.of.year'})
    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(Tmp_DF)
    OutputTableAnnual_DF = OutputTableAnnual_DF.rename(
        columns={'coincidence.factor.equipment':'coincidence.factor.system'})

    OutputTableAnnual_DF['demand.electricity.source.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.source.total.stock']*
        OutputTableAnnual_DF['coincidence.factor.system']/0.0036)

    OutputTableAnnual_DF['energy.gas.site.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['energy.gas.site.per.unit'])
    OutputTableAnnual_DF['energy.oil.site.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['energy.oil.site.per.unit'])
    OutputTableAnnual_DF['energy.propane.site.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['energy.propane.site.per.unit'])
    OutputTableAnnual_DF['energy.biomass.site.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['energy.biomass.site.per.unit'])
    OutputTableAnnual_DF['energy.electricity.site.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['energy.electricity.site.per.unit'])

    OutputTableAnnual_DF['demand.electricity.site.new.stock'] = (
        OutputTableAnnual_DF['energy.electricity.site.new.stock']*
        OutputTableAnnual_DF['coincidence.factor.customer']/0.0036)

    OutputTableAnnual_DF['energy.gas.source.new.stock'] = (
        OutputTableAnnual_DF['energy.gas.site.new.stock']/
        (1.0 - OutputTableAnnual_DF['system_loss.gas']))
    OutputTableAnnual_DF['energy.oil.source.new.stock'] = OutputTableAnnual_DF['energy.oil.site.new.stock']
    OutputTableAnnual_DF['energy.propane.source.new.stock'] = OutputTableAnnual_DF['energy.propane.site.new.stock']
    OutputTableAnnual_DF['energy.biomass.source.new.stock'] = OutputTableAnnual_DF['energy.biomass.site.new.stock']
    OutputTableAnnual_DF['energy.electricity.source.new.stock'] = (
        OutputTableAnnual_DF['energy.electricity.site.new.stock']/
        (1.0 - OutputTableAnnual_DF['system_loss.electricity']))

    OutputTableAnnual_DF['demand.electricity.source.new.stock'] = (
        OutputTableAnnual_DF['energy.electricity.source.new.stock']*
        OutputTableAnnual_DF['coincidence.factor.system']/0.0036)

    OutputTableAnnual_DF['cost.customer.real.annual.upfront.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*OutputTableAnnual_DF['equipment.upfront.cost'])
    OutputTableAnnual_DF['cost.customer.real.annual.maintenance.total.stock'] = (
        OutputTableAnnual_DF['equipment.stock.total']*OutputTableAnnual_DF['equipment.maintenance.cost'])

    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(RetailRates_DF)
    OutputTableAnnual_DF['cost.customer.real.annual.energy.gas.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.site.total.stock']*OutputTableAnnual_DF['retail_rate.gas.energy'])
    OutputTableAnnual_DF['cost.customer.real.annual.energy.oil.total.stock'] = (
        OutputTableAnnual_DF['energy.oil.site.total.stock']*OutputTableAnnual_DF['retail_rate.oil.energy'])
    OutputTableAnnual_DF['cost.customer.real.annual.energy.propane.total.stock'] = (
        OutputTableAnnual_DF['energy.propane.site.total.stock']*OutputTableAnnual_DF['retail_rate.propane.energy'])
    OutputTableAnnual_DF['cost.customer.real.annual.energy.biomass.total.stock'] = (
        OutputTableAnnual_DF['energy.biomass.site.total.stock']*OutputTableAnnual_DF['retail_rate.biomass.energy'])
    OutputTableAnnual_DF['cost.customer.real.annual.energy.electricity.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.site.total.stock']*
        OutputTableAnnual_DF['retail_rate.electricity.energy'])
    OutputTableAnnual_DF['cost.customer.real.annual.demand.electricity.total.stock'] = (
        OutputTableAnnual_DF['demand.electricity.site.total.stock']*
        OutputTableAnnual_DF['retail_rate.electricity.demand'])

    OutputTableAnnual_DF['cost.customer.real.annual.ongoing.total.stock'] = (
        OutputTableAnnual_DF['cost.customer.real.annual.maintenance.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.energy.gas.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.energy.oil.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.energy.propane.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.energy.biomass.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.energy.electricity.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.demand.electricity.total.stock'])
    OutputTableAnnual_DF['cost.customer.real.annual.total.stock'] = (
        OutputTableAnnual_DF['cost.customer.real.annual.upfront.total.stock'] + 
        OutputTableAnnual_DF['cost.customer.real.annual.ongoing.total.stock'])
    OutputTableAnnual_DF['cost.resource.real.annual.upfront.total.stock'] = (
        OutputTableAnnual_DF['cost.customer.real.annual.upfront.total.stock'])
    OutputTableAnnual_DF['cost.resource.real.annual.maintenance.total.stock'] = (
        OutputTableAnnual_DF['cost.customer.real.annual.maintenance.total.stock'])

    OutputTableAnnual_DF = OutputTableAnnual_DF.merge(ResourceCosts_DF)
    OutputTableAnnual_DF['cost.resource.real.annual.energy.gas.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.source.total.stock']*OutputTableAnnual_DF['resource_cost.gas.energy'])
    OutputTableAnnual_DF['cost.resource.real.annual.energy.oil.total.stock'] = (
        OutputTableAnnual_DF['energy.oil.source.total.stock']*OutputTableAnnual_DF['resource_cost.oil.energy'])
    OutputTableAnnual_DF['cost.resource.real.annual.energy.propane.total.stock'] = (
        OutputTableAnnual_DF['energy.propane.source.total.stock']*OutputTableAnnual_DF['resource_cost.propane.energy'])
    OutputTableAnnual_DF['cost.resource.real.annual.energy.biomass.total.stock'] = (
        OutputTableAnnual_DF['energy.biomass.source.total.stock']*OutputTableAnnual_DF['resource_cost.biomass.energy'])
    OutputTableAnnual_DF['cost.resource.real.annual.energy.electricity.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.source.total.stock']*
        OutputTableAnnual_DF['resource_cost.electricity.energy'])
    OutputTableAnnual_DF['cost.resource.real.annual.demand.electricity.total.stock'] = (
        OutputTableAnnual_DF['demand.electricity.source.total.stock']*
        OutputTableAnnual_DF['resource_cost.electricity.demand'])

    OutputTableAnnual_DF['cost.resource.real.annual.ongoing.total.stock'] = (
        OutputTableAnnual_DF['cost.resource.real.annual.maintenance.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.energy.gas.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.energy.oil.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.energy.propane.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.energy.biomass.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.energy.electricity.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.demand.electricity.total.stock'])
    OutputTableAnnual_DF['cost.resource.real.annual.total.stock'] = (
        OutputTableAnnual_DF['cost.resource.real.annual.upfront.total.stock'] +
        OutputTableAnnual_DF['cost.resource.real.annual.ongoing.total.stock'])
    OutputTableAnnual_DF['cost.customer.pv.upfront.new.stock'] = (
        OutputTableAnnual_DF['cost.customer.real.annual.upfront.total.stock'])

    OutputTableAnnual_DF['cost.customer.pv.lifetime.maintenance.new.stock'] = (
        OutputTableAnnual_DF['equipment.stock.new']*
        -pv(OutputTableAnnual_DF['discount.rate.customer'],
           OutputTableAnnual_DF['equipment.EUL'],
           OutputTableAnnual_DF['equipment.maintenance.cost']))


    OutputTableAnnual_DF['energy.gas.losses.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.source.total.stock'] - OutputTableAnnual_DF['energy.gas.site.total.stock'])

    OutputTableAnnual_DF['ghg.electricity.production.total.stock'] = (
        OutputTableAnnual_DF['energy.electricity.source.total.stock']*
        OutputTableAnnual_DF['emissions_factor.electricity']/1000000)
    OutputTableAnnual_DF['ghg.gas.consumption.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.site.total.stock']*
        OutputTableAnnual_DF['emissions_factor.gas']/1000000)
    OutputTableAnnual_DF['ghg.gas.losses.total.stock'] = (
        OutputTableAnnual_DF['energy.gas.losses.total.stock']*
        OutputTableAnnual_DF['emissions_factor.gas.leakage']/1000000)
    OutputTableAnnual_DF['ghg.oil.consumption.total.stock'] = (
        OutputTableAnnual_DF['energy.oil.site.total.stock']*
        OutputTableAnnual_DF['emissions_factor.oil']/1000000)
    OutputTableAnnual_DF['ghg.propane.consumption.total.stock'] = (
        OutputTableAnnual_DF['energy.propane.site.total.stock']*
        OutputTableAnnual_DF['emissions_factor.propane']/1000000)
    OutputTableAnnual_DF['ghg.biomass.consumption.total.stock'] = (
        OutputTableAnnual_DF['energy.biomass.site.total.stock']*
        OutputTableAnnual_DF['emissions_factor.biomass']/1000000)

    OutputTableAnnual_DF['ghg.direct.total.stock'] = (
        OutputTableAnnual_DF['ghg.gas.consumption.total.stock'] +
        OutputTableAnnual_DF['ghg.oil.consumption.total.stock'] +
        OutputTableAnnual_DF['ghg.propane.consumption.total.stock'] +
        OutputTableAnnual_DF['ghg.biomass.consumption.total.stock'])
    OutputTableAnnual_DF['ghg.indirect.total.stock'] = (
        OutputTableAnnual_DF['ghg.electricity.production.total.stock'] +
        OutputTableAnnual_DF['ghg.gas.losses.total.stock'])
    OutputTableAnnual_DF['ghg.total.stock'] = (
        OutputTableAnnual_DF['ghg.direct.total.stock'] +
        OutputTableAnnual_DF['ghg.indirect.total.stock'])

    OutputTableAnnual_DF.to_csv('Output table annual.csv',index=False)
    pass

# Output table hourly
OutputTableHourly_DF = OutputTableAnnual_DF[
    ['province.territory','sector','equipment.type','equipment.load.shape','forecast.year',
     'energy.electricity.source.total.stock']]
OutputTableHourly_DF = OutputTableHourly_DF.groupby(
    ['province.territory','sector','equipment.type','equipment.load.shape','forecast.year']).sum().reset_index()

OutputTableHourly_DF = OutputTableHourly_DF.merge(EquipmentLoadShapes_DF)

OutputTableHourly_DF['demand.electricity.source.total.stock'] = (
    OutputTableHourly_DF['energy.electricity.source.total.stock']*
    OutputTableHourly_DF['coincidence.factor.equipment']/0.0036)

OutputTableHourly_DF = OutputTableHourly_DF[
    ['province.territory','sector','equipment.type',
     'forecast.year','hour.of.year','demand.electricity.source.total.stock']]
OutputTableHourly_DF = OutputTableHourly_DF.groupby(
    ['province.territory','sector','equipment.type','forecast.year','hour.of.year']).sum().reset_index()

OutputTableHourly_DF = pd.pivot_table(
    OutputTableHourly_DF,
    index=['province.territory','sector','equipment.type','hour.of.year'],
    columns='forecast.year',
    values='demand.electricity.source.total.stock').reset_index()

OutputTableHourly_DF.to_csv('Output table hourly.csv',index=False)
